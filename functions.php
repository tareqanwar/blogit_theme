<?php
/*------------------------------------*\
	Functions
\*------------------------------------*/

// BlogIt menus
function blogit_menus() {
    $defaults = array(
        'theme_location'  => 'main-menu',
    );
    
    wp_nav_menu( $defaults );
}



// Load styles & scripts
function register_blogit_scripts() {
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), 20141119 );
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/style.css', array(), 20141119 );
}
add_action( 'wp_enqueue_scripts', 'register_blogit_scripts' );

?>