<?php get_header(); ?>

<div id="primary">

<?php if (have_posts()): while (have_posts()) : the_post(); ?>			
    <article>
        
        <h1><a href="<?=the_permalink();?>"><?=the_title();?></a></h1>

        <?=the_content(); ?>

    </article><!-- /article -->

    <?php endwhile; ?>

    <?php else: ?>

    <!-- article -->
    <article>
        <h2><?php _e( 'Sorry, nothing to display.', 'afterschool' ); ?></h2>
    </article>
    <!-- /article -->

    <?php endif; ?>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
