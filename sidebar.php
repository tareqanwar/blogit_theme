<div id="secondary">

    <h3>Menu</h3>
    
    <?php blogit_menus(); ?>

    <h3>Archive</h3>

    <ul class="menu">
        <?php wp_get_archives( array( 'type' => 'monthly') ); ?>
    </ul>

</div><!-- /secondary -->